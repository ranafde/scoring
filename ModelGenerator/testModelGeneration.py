#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 24 20:27:33 2017

@author: rahulnafde
"""

import pandas
from sklearn import tree
from sklearn_pandas import DataFrameMapper
from sklearn2pmml import PMMLPipeline
from sklearn.preprocessing import StandardScaler
from sklearn2pmml.decoration import ContinuousDomain
from sklearn.externals import joblib

iris_df = pandas.read_csv("/Users/rahulnafde/Documents/HBC/Solution/iris.csv")
#iris_mapper = DataFrameMapper([ (["sepal_length", "sepal_width", "petal_length", "petal_width"], [ContinuousDomain(), StandardScaler()])])
iris_mapper = DataFrameMapper([ (["sepal_length", "sepal_width", "petal_length", "petal_width"], [ContinuousDomain(), StandardScaler()])])
iris_classifier = tree.DecisionTreeClassifier(min_samples_leaf = 5)
iris_pipeline = PMMLPipeline([
     ("mapper", iris_mapper),
    ("estimator", iris_classifier)
])
iris_pipeline.fit(iris_df, iris_df["species"])
joblib.dump(iris_pipeline, "/Users/rahulnafde/Documents/HBC/Solution/pipeline1.pkl.z", compress = 9)