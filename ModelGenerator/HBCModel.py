#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 26 12:49:45 2017

@author: rahulnafde
"""

import pandas as pd
from sklearn import tree
from sklearn_pandas import DataFrameMapper
from sklearn2pmml import PMMLPipeline
from sklearn.preprocessing import StandardScaler, LabelBinarizer, LabelEncoder
from sklearn2pmml.decoration import ContinuousDomain
from sklearn.externals import joblib
from sklearn.base import TransformerMixin

'''
class ConvertToDate(TransformerMixin):
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        dt = pd.to_datetime(X)
        print dt
        return pd.concat([dt.time], axis=1)
'''    
'''
class DateEncoder(TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X):
        dt = X.dt
        return pd.concat([dt.time], axis=1)
'''

class GetDifference(TransformerMixin):
    def __init__(self, x, y):
        self.x = x;
        self.y = y;
        
    def fit(self, X, y=None):
        return self;
    
    def transform(self, X):
        d = self.x - self.y;
        return pd.concat([d], axis=1);
    
# read the raw data
data  = pd.read_csv('/Users/rahulnafde/Documents/HBC/Solution/original.csv');

# pre-processing
data_mapper = DataFrameMapper([ 
        ('product_look_id', [LabelEncoder()]),
        ('sale_id', [LabelEncoder()]), 
#        ('sale_start_time', [ConvertToDate()]),
        ('brand_id', [LabelBinarizer()]),
        ('sale_type_key', [LabelBinarizer()]),
        ('shoe_type', [LabelBinarizer()]),
        ('return_policy_id', [LabelBinarizer()]),
        ('material_name', [LabelBinarizer()]),
        ('color', [LabelBinarizer()]), 
        ('country_of_origin', [LabelBinarizer()]),
        (['num_units_available'], [ContinuousDomain(), StandardScaler()]),
        (['msrp_price'], [ContinuousDomain(), StandardScaler()]),
        (['base_price'], [ContinuousDomain(), StandardScaler()]),
        (['initial_sale_price'], [ContinuousDomain(), StandardScaler()]),   
        (['last_price'], [ContinuousDomain(), StandardScaler()]),
        (['num_sizes'], [ContinuousDomain(), StandardScaler()]),
#        (['msrp_price', 'base_price'], [GetDifference(data['msrp_price'],data['base_price'])], {'alias':'difference_msrp_base_price'})
        ])

#data['difference_msrp_base_price'] = data['msrp_price'] - data['base_price'];
#data['difference_intial_last_price'] = data['initial_sale_price'] - data['last_price'];

# Create Estimator
data_classifier = tree.DecisionTreeClassifier(min_samples_leaf = 5)

# PMML Pipeline
data_pipeline = PMMLPipeline([
     ('mapper', data_mapper),
    ('estimator', data_classifier)
])


data_pipeline.fit(data, data['num_units_sold'])
joblib.dump(data_pipeline, '/Users/rahulnafde/Documents/HBC/Solution/pipeline2.pkl.z', compress = 9)