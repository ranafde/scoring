# Model Generator

Python scripts listed below are used to generate a model in pickle format which is subsequently converted to PMML format

# Model Generation steps

1. Manually inspected data to identify the data type of the features - Continuous, Categorical, Boolean.

1. Check for null, empty or missing values in the dataset provided - In case of a missing value - value can be imputed (using various techniques like min, max, most frequent value) or the row could be discarded (but we might loose data). For the given dataset, there were no missing values.

1. After data-clean up, the pre-processing step involved identifying/generating features which have high mutual information with the goal `num_units_sold`

# Model Evaluation

1. Different types of learning techniques explored using the technique listed below - RandomForest, NeuralNets, LinearRegression, SVM. RandomForest gave the best (lowest Mean Squared Error Rate) result.

1. To evaluate model performance, 10-fold cross validation was used (steps listed below):
   1. Pre-processed data is split into training and testing (80/20)
   1. Training data was further split into 10-fold (k-fold). Training was performed 10 times (k) on 9 folds (k-1) with one fold retained for validation set

1. After comparing the results of the previous step with multiple models, the best amongst them was finalized.

*Note:* Mean Squared Error Rate was used as the metric for evaluation as the target column (`num_units_sold`) is CONTINUOUS


# Converting a pickle file to PMML

Models generated in python *pickle* format can be converted to the PMML format by using `jpmml-sklearn`

`java -jar ./target/converter-executable-1.4-SNAPSHOT.jar --pkl-input /Users/rahulnafde/Documents/HBC/Solution/pipeline2.pkl.z --pmml-output /Users/rahulnafde/Documents/HBC/Solution/pipeline2.pmml`


# References:

Open-source software(s) used is listed below

1. jpmml-sklearn (https://github.com/jpmml/jpmml-sklearn) - Java library and command-line application for converting Scikit-Learn models to PMML

1. pandas for python (https://pandas.pydata.org/) - Data Analysis Library 