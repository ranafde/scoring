#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 17:20:40 2017

@author: rahulnafde
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import cross_validation
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn import svm
from sklearn.ensemble import AdaBoostClassifier
from sklearn import model_selection
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold

# read the raw data
data  = pd.read_csv("/Users/rahulnafde/Documents/HBC/Solution/original.csv");

# clean up
# convert to appropriate data types
data['product_look_id'] = data.product_look_id.astype('category');
data['sale_id'] = data.sale_id.astype('category');
data['brand_id'] = data.brand_id.astype('category');
data['sale_start_time'] = pd.to_datetime(data['sale_start_time']);
data['sale_end_time'] = pd.to_datetime(data['sale_end_time']);
data['sale_type_key'] = data.sale_type_key.astype('category');
data['shoe_type'] = data.shoe_type.astype('category');
data['return_policy_id'] = data.return_policy_id.astype('category');
data['material_name'] = data.material_name.astype('category');
data['color'] = data.color.astype('category');
data['country_of_origin'] = data.country_of_origin.astype('category');

# Data transformations
#1 convert datetime to time
data['sale_start_only_time'] = pd.Series([val.time() for val in data['sale_start_time']]);
data['sale_end_only_time'] = pd.Series([val.time() for val in data['sale_end_time']]);
data['sale_start_only_time_code'] = data.sale_start_only_time.astype('category');
data['sale_end_only_time_code'] = data.sale_end_only_time.astype('category');
data['difference_msrp_base_price'] = data['msrp_price'] - data['base_price'];
data['difference_intial_last_price'] = data['initial_sale_price'] - data['last_price'];
#print (data.info())

#2 convert to categorical encoding
cat_columns = data.select_dtypes(['category']).columns;
print (cat_columns);
data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes);

#3 remove unwanted columns 
columns = data.columns.tolist();
columns = [c for c in columns if c not in ['sale_start_time','sale_end_time','sale_start_only_time','sale_end_only_time','id']];
res = data.filter(columns);
#print res.info()

#uncomment below to view the file
res.to_csv('/Users/rahulnafde/Documents/HBC/Solution/out.csv', sep=',', encoding='utf-8');

# Training and validation
target = 'num_units_sold'
seed = 2
# get all the columns from the dataframe.
columns= res.columns.tolist();
columns = [c for c in columns if c not in ['num_units_sold']];

# Generate the training set.  Set random_state to be able to replicate results.
train = res.sample(frac=0.8, random_state=seed)
# Select anything not in the training set and put it in the testing set.
test = res.loc[~res.index.isin(train.index)]

# Initialize the model class
# Linear regression
#model = LinearRegression()
# svm [25.36x]
#model = svm.SVC()
# random forest [8.56x]
#model = RandomForestRegressor(n_estimators=100, min_samples_leaf=10, random_state=seed)
# AdaBoost [26.52x]
#model = AdaBoostClassifier(n_estimators=30, random_state=seed)
#model.fit(train[columns], train[target])


len_training_data = len(train)
print len(train)
n_fold = 10;
fold_size  = len_training_data / n_fold;
start = 0
model = RandomForestRegressor(n_estimators=400, min_samples_leaf=10, random_state=seed)
for i in range (0,n_fold):
    if(i == n_fold-1) :
        end = len_training_data - 1; 
    else :
        end = start + fold_size
    
    #print str(start) + '-' + str(end)

    validation_set = train[start:end]
    cv_train_set = train.loc[~train.index.isin(validation_set.index)]
    model.fit(cv_train_set[columns], cv_train_set[target])
    # Generate our predictions for the test set.
    predictions = model.predict(validation_set[columns])
    # Compute error between our test predictions and the actual values.
    print(mean_squared_error(predictions, validation_set[target]))
    start = end
 
# Test
print 'Result'

predictions_test = model.predict(test[columns])
print(mean_squared_error(predictions_test, test[target]))

# plot predictions and actual
#plt.scatter(test[target], predictions_test)
#plt.xlabel('True Values')
#plt.ylabel('Predictions')
