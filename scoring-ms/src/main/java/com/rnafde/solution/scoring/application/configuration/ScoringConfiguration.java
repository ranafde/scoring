package com.rnafde.solution.scoring.application.configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

public class ScoringConfiguration extends Configuration {

	@NotEmpty
	private String dataDir;

	@Valid
	@NotNull
	private DataSourceFactory database = new DataSourceFactory();

	@JsonProperty
	public void setDataDir(final String dataDir) {
		this.dataDir = dataDir;
	}

	@JsonProperty
	public String getDataDir() {
		return dataDir;
	}

	@JsonProperty("database")
	public void setDataSourceFactory(final DataSourceFactory factory) {
		this.database = factory;
	}

	@JsonProperty("database")
	public DataSourceFactory getDataSourceFactory() {
		return database;
	}
}
