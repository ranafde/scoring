package com.rnafde.solution.scoring.application;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.skife.jdbi.v2.DBI;

import com.rnafde.scoring.db.dao.ModelDAO;
import com.rnafde.scoring.resource.ModelResource;
import com.rnafde.scoring.resource.ScoringHealthCheck;
import com.rnafde.scoring.resource.ScoringResource;
import com.rnafde.scoring.service.ModelService;
import com.rnafde.scoring.service.ScoringService;
import com.rnafde.solution.scoring.application.configuration.ScoringConfiguration;

import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class ScoringApplication extends Application<ScoringConfiguration> {

	public static void main(final String[] args) throws Exception {
		new ScoringApplication().run(args);
	}

	@Override
	public String getName() {
		return "Scoring Application";
	}

	@Override
	public void initialize(final Bootstrap<ScoringConfiguration> bootstrap) {
		// nothing to do yet
	}

	@Override
	public void run(final ScoringConfiguration configuration, final Environment environment) {

		final DBIFactory factory = new DBIFactory();
		final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "h2");
		final ModelDAO modelDAO = jdbi.onDemand(ModelDAO.class);
		modelDAO.createModelTable();
		final ModelService modelService = new ModelService(configuration.getDataDir());
		final ScoringService scoringService = new ScoringService(configuration.getDataDir());
		final ModelResource modelResource = new ModelResource(modelService, modelDAO);
		final ScoringResource scoringResource = new ScoringResource(scoringService);
		final ScoringHealthCheck scoringHealthCheck = new ScoringHealthCheck();

		environment.healthChecks().register("Scoring Health Check", scoringHealthCheck);
		environment.jersey().register(modelResource);
		environment.jersey().register(scoringResource);
		environment.jersey().register(MultiPartFeature.class);
	}
}
