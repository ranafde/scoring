package com.rnafde.scoring.resource;

import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.rnafde.scoring.db.dao.ModelDAO;
import com.rnafde.scoring.dto.ModelInfo;
import com.rnafde.scoring.service.ModelService;

@Path("/models")
@Produces(MediaType.APPLICATION_JSON)
public class ModelResource {
	private final ModelService modelService;
	private final ModelDAO modelDAO;

	public ModelResource(final ModelService modelService, final ModelDAO modelDAO) {
		this.modelService = modelService;
		this.modelDAO = modelDAO;
	}

	@GET
	public List<ModelInfo> getModelInfos() {
		final List<ModelInfo> modelInfos = modelDAO.listModels();
		return modelInfos;
	}

	@GET
	@Path("{id}")
	public ModelInfo getModelInfo(@PathParam("id") final Integer id) {
		final ModelInfo modelInfo = modelDAO.findModelById(id);
		return modelInfo;
	}

	@GET
	@Path("{id}/download")
	@Produces(MediaType.APPLICATION_XML)
	public Response getModel(@PathParam("id") final String modelId) {
		return Response.ok(modelService.getModelById(modelId)).build();
	}

	@Path("/upload")
	@PUT
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadModel(@FormDataParam("file") final InputStream uploadedInputStream,
			@FormDataParam("file") final FormDataContentDisposition fileDetail,
			@FormDataParam("name") final String name, @FormDataParam("description") final String description) {
		final int id = modelDAO.insert(name, description);
		modelService.saveModel(uploadedInputStream, id);
		final ModelInfo modelInfo = new ModelInfo(id, name, description);
		return Response.status(200).entity(modelInfo).build();
	}

	@DELETE
	@Path("{id}")
	public Response deleteModel(@PathParam("id") final Integer modelId) {
		modelDAO.deleteById(modelId);
		modelService.deleteModelById(modelId);
		return Response.status(200).build();
	}
}
