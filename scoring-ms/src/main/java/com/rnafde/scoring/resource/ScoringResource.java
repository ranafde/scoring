package com.rnafde.scoring.resource;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import com.rnafde.scoring.dto.ScoringRequest;
import com.rnafde.scoring.dto.ScoringResponse;
import com.rnafde.scoring.service.ScoringService;

@Path("/score")
@Produces(MediaType.APPLICATION_JSON)
public class ScoringResource {
	private final ScoringService scoringService;

	public ScoringResource(final ScoringService scoringService) {
		this.scoringService = scoringService;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ScoringResponse scoreSynchronous(final ScoringRequest request)
			throws WebApplicationException, SAXException, JAXBException, IOException {
		return scoringService.scoreSynchronously(request);
	}
}
