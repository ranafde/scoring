package com.rnafde.scoring.resource;

import com.codahale.metrics.health.HealthCheck;

public class ScoringHealthCheck extends HealthCheck {

	@Override
	protected Result check() throws Exception {
		// TODO: check if db is accessible
		return Result.healthy();
	}

}
