package com.rnafde.scoring.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ModelInfo {
	private int id;
	private String name;
	private String description;

	// for jackson
	private ModelInfo() {
	}

	public ModelInfo(final int id, final String name, final String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	@JsonProperty
	public long getId() {
		return id;
	}

	@JsonProperty
	public String getName() {
		return name;
	}

	@JsonProperty
	public String getDescription() {
		return description;
	}

}
