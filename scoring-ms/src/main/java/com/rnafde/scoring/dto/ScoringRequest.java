package com.rnafde.scoring.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class ScoringRequest {

	private List<ScoringRow> rows = new ArrayList<>();
	private String modelId;

	private ScoringRequest() {
	}

	public List<ScoringRow> getRows() {
		return rows;
	}

	public void setArguments(final List<ScoringRow> rows) {
		this.rows = rows;
	}

	public String getModelId() {
		return modelId;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("modelId:").append(modelId).append("\n");
		for (final ScoringRow row : rows) {
			sb.append(row);
		}
		return sb.toString();
	}
}
