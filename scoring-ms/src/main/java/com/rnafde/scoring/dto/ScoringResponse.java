package com.rnafde.scoring.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class ScoringResponse {
	// private Map<String, ?> scores;
	// private String id;
	private List<ScoringRow> rows = new ArrayList<>();

	public ScoringResponse() {
	}

	public List<ScoringRow> getRows() {
		return this.rows;
	}

	public void setRows(final List<ScoringRow> rows) {
		this.rows = rows;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		for (final ScoringRow row : rows) {
			sb.append(row.toString());
		}
		return sb.toString();
	}
}
