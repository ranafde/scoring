package com.rnafde.scoring.dto;

import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class ScoringRow {
	private Map<String, ?> features;
	private String id;

	public ScoringRow() {
	}

	public void setFeatures(final Map<String, ?> row) {
		this.features = row;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public Map<String, ?> getFeatures() {
		return features;
	}

	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("id:").append(id).append("\n");
		for (final Entry<String, ?> kv : features.entrySet()) {
			sb.append(kv.getKey() + ":").append(kv.getValue()).append("\n");
		}
		return sb.toString();
	}
}
