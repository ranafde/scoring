package com.rnafde.scoring.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

public class ModelService {
	private static Logger LOGGER = Logger.getLogger(ModelService.class);

	private final String modelsLocation;

	public ModelService(final String modelsLocation) {
		this.modelsLocation = modelsLocation;
	}

	public void saveModel(final InputStream inputStream, final int id) {
		LOGGER.info("Saving model file");
		final File writeLocation = new File(modelsLocation);
		if (!writeLocation.exists()) {
			writeLocation.mkdirs();
		}
		final String fileName = String.valueOf(id);
		try (Writer writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(modelsLocation + fileName), "utf-8"))) {
			IOUtils.copy(inputStream, writer, StandardCharsets.UTF_8);
		} catch (final IOException e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
	}

	public StreamingOutput getModelById(final String modelId) throws WebApplicationException {
		final StreamingOutput modelStream = new StreamingOutput() {
			@Override
			public void write(final OutputStream os) throws IOException, WebApplicationException {
				try {
					final FileInputStream inputStream = new FileInputStream(new File(modelsLocation + modelId));
					final Writer writer = new BufferedWriter(new OutputStreamWriter(os));
					IOUtils.copy(inputStream, writer, StandardCharsets.UTF_8);
					writer.flush();
					writer.close();
				} catch (final FileNotFoundException fnfe) {
					LOGGER.error(fnfe.getMessage());
					throw new WebApplicationException(Response.Status.NOT_FOUND);
				}
			}
		};
		return modelStream;
	}

	public void deleteModelById(final Integer modelId) throws WebApplicationException {
		try {
			LOGGER.info("Deleting file");
			final String modelPath = modelsLocation + modelId;
			Files.delete(new File(modelPath).toPath());
		} catch (final IOException io) {
			LOGGER.error(io.getMessage());
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
}
