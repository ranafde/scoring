package com.rnafde.scoring.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import javax.ws.rs.WebApplicationException;
import javax.xml.bind.JAXBException;

import org.dmg.pmml.Entity;
import org.dmg.pmml.FieldName;
import org.dmg.pmml.PMML;
import org.jpmml.evaluator.Computable;
import org.jpmml.evaluator.Evaluator;
import org.jpmml.evaluator.FieldValue;
import org.jpmml.evaluator.HasEntityId;
import org.jpmml.evaluator.HasEntityRegistry;
import org.jpmml.evaluator.HasProbability;
import org.jpmml.evaluator.InputField;
import org.jpmml.evaluator.InvalidResultException;
import org.jpmml.evaluator.ModelEvaluator;
import org.jpmml.evaluator.ModelEvaluatorFactory;
import org.jpmml.evaluator.OutputField;
import org.jpmml.evaluator.TargetField;
import org.xml.sax.SAXException;

import com.google.common.collect.BiMap;
import com.rnafde.scoring.dto.ScoringRequest;
import com.rnafde.scoring.dto.ScoringResponse;
import com.rnafde.scoring.dto.ScoringRow;

public class ScoringService {
	private final String modelsLocation;

	public ScoringService(final String modelsLocation) {
		this.modelsLocation = modelsLocation;
	}

	public BiFunction<Evaluator, ScoringRow, ScoringRow> scoreRow = (evaluator, row) -> {
		try {
			final Map<FieldName, FieldValue> arguments = new LinkedHashMap<>();
			final List<InputField> inputFields = evaluator.getInputFields();

			for (final InputField inputField : inputFields) {
				final FieldName inputFieldName = inputField.getName();
				// The raw (ie. user-supplied) value could be any Java
				// primitive value
				final Object rawValue = row.getFeatures().get(inputFieldName.getValue());

				// The raw value is passed through: 1) outlier treatment, 2)
				// missing value treatment, 3) invalid value treatment and 4)
				// type conversion
				final FieldValue inputFieldValue = inputField.prepare(rawValue);

				arguments.put(inputFieldName, inputFieldValue);
			}

			final Map<FieldName, ?> results = evaluator.evaluate(arguments);
			final Map<String, Object> scores = new HashMap<>();
			final ScoringRow resultRow = new ScoringRow();

			final List<TargetField> targetFields = evaluator.getTargetFields();
			for (final TargetField targetField : targetFields) {
				final FieldName targetFieldName = targetField.getName();
				final Object targetFieldValue = results.get(targetFieldName);
				if (targetFieldValue instanceof Computable) {
					final Computable computable = (Computable) targetFieldValue;

					final Object unboxedTargetFieldValue = computable.getResult();
					scores.put(targetFieldName.getValue(), unboxedTargetFieldValue);
				}

				if (targetFieldValue instanceof HasEntityId) {
					final HasEntityId hasEntityId = (HasEntityId) targetFieldValue;
					final HasEntityRegistry<?> hasEntityRegistry = (HasEntityRegistry<?>) evaluator;
					final BiMap<String, ? extends Entity> entities = hasEntityRegistry.getEntityRegistry();
					final Entity winner = entities.get(hasEntityId.getEntityId());

					// Test for "probability" result feature
					if (targetFieldValue instanceof HasProbability) {
						final HasProbability hasProbability = (HasProbability) targetFieldValue;
						hasProbability.getProbability(winner.getId());
					}
				}
			}

			final List<OutputField> outputFields = evaluator.getOutputFields();
			for (final OutputField outputField : outputFields) {
				final FieldName outputFieldName = outputField.getName();
				results.get(outputFieldName);
			}
			resultRow.setFeatures(scores);
			resultRow.setId(row.getId());
			return resultRow;
		} catch (final Exception e) {
			final ScoringRow resultRow = new ScoringRow();
			resultRow.setId(row.getId());
			final Map<String, String> features = new HashMap<>();
			if (e instanceof InvalidResultException) {
				features.put("errorMessage", "Unable to score this record, invalid data");
			}
			resultRow.setFeatures(features);
			return resultRow;
		}
	};

	public ScoringResponse scoreSynchronously(final ScoringRequest request)
			throws SAXException, JAXBException, WebApplicationException, IOException {
		try (InputStream modelInputStream = new FileInputStream(new File(modelsLocation + request.getModelId()))) {
			final PMML pmml = org.jpmml.model.PMMLUtil.unmarshal(modelInputStream);
			final ModelEvaluatorFactory modelEvaluatorFactory = ModelEvaluatorFactory.newInstance();
			final ModelEvaluator<?> modelEvaluator = modelEvaluatorFactory.newModelEvaluator(pmml);
			final Evaluator evaluator = modelEvaluator;
			final ScoringResponse response = new ScoringResponse();
			final List<ScoringRow> results = request.getRows().stream().map(row -> scoreRow.apply(evaluator, row))
					.collect(Collectors.toList());
			response.setRows(results);
			return response;
		}
	}

}
