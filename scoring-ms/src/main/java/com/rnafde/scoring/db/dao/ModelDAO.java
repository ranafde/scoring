package com.rnafde.scoring.db.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.rnafde.scoring.db.mapper.ModelInfoMapper;
import com.rnafde.scoring.dto.ModelInfo;

@RegisterMapper(ModelInfoMapper.class)
public interface ModelDAO {

	@SqlUpdate("create table MODEL (id int primary key auto_increment, name varchar(100), description varchar(500))")
	void createModelTable();

	@SqlUpdate("insert into MODEL (name, description) values (:name, :description)")
	@GetGeneratedKeys
	public int insert(@Bind("name") String name, @Bind("description") String description);

	@SqlQuery("select * from MODEL where id = :id")
	public ModelInfo findModelById(@Bind("id") int id);

	@SqlUpdate("delete from MODEL where id = :id")
	public int deleteById(@Bind("id") int id);

	@SqlQuery("SELECT * FROM MODEL ORDER BY id")
	List<ModelInfo> listModels();
}
