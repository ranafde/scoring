package com.rnafde.scoring.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.rnafde.scoring.dto.ModelInfo;

public class ModelInfoMapper implements ResultSetMapper<ModelInfo> {

	@Override
	public ModelInfo map(final int index, final ResultSet resultSet, final StatementContext statementContext)
			throws SQLException {
		return new ModelInfo(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("description"));
	}
}
