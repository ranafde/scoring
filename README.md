# Scoring Microservice

# Usage

The scoring microservice is RESTful web service which can be used to score data synchronously (currently) using a PMML model. The user can bring multiple models and upload them to the microservice. While scoring rows, the user should reference model via its *id*

- Input is a set of data rows in JSON format to be scored and the model in PMML format which will be used for scoring
- Output is a set of rows which correspond to scores per row

## REST APIs

### Model management

1. Get Model Info - `localhost:8080/models/{id}`
	Type: GET
	Output: Model info describing the model
	<pre>
	{
	    "id": 1,
	    "name": "modelHBC",
	    "description": "HBC test"
	}
	</pre>


1. Model Upload - `localhost:8080/models/upload`
	Type: PUT
	Input: Form data containing name, description, PMML model file
	Output: Returns a ModelInfo which contains the modelId to be used later for scoring
	<pre>
	{
	    "id": 2,
	    "name": "modelHBC",
	    "description": "hbc tree "
	}
	</pre>

1. List all models - `localhost:8080/models/`
	Type: GET
	Output: List of uploaded models with their modelInfo
	<pre>
	[
	    {
	        "id": 1,
	        "name": "modelHBC",
	        "description": "hbc tree "
	    },
	    {
	        "id": 2,
	        "name": "modelIris",
	        "description": "Iris Test"
	    }
	]
	</pre>

1. Get or Download Model - `localhost:8080/models/{id}`
	Type: GET
	Output: Returns the model file (xml)

1. Delete Model - `localhost:8080/models/{id}`
	Type: DELETE


### Scoring

1. score - `localhost:8080/score`
	Type: POST
	Input: List of rows to be scored as a JSON
	<pre>
	{
	  "modelId": "2",
	  "rows": [
	    {
	      "id": "4",
	      "features": {
	        "product_look_id": 1013583601,
	        "sale_id": 1059839361,
	        "sale_type_key": "single-brand",
	        "brand_id": "b146",
	        "shoe_type": "pumps",
	        "return_policy_id": "r-1",
	        "base_price": 29.4,
	        "sale_price": 59,
	        "msrp_price": 100,
	        "initial_sale_price": 59,
	        "last_price": 59,
	        "material_name": "snakembossedleather",
	        "color": "Silver",
	        "country_of_origin": "CHN",
	        "num_units_available": 2
	      }
	    },
	    {
	      "id": "14",
	      "features": {
	        "product_look_id": 1013583601,
	        "sale_id": 1089811882,
	        "sale_type_key": "multi-brand",
	        "brand_id": "b88",
	        "shoe_type": "bootie",
	        "return_policy_id": "r-1",
	        "base_price": 129.4,
	        "sale_price": 159,
	        "msrp_price": 100,
	        "initial_sale_price": 159,
	        "last_price": 59,
	        "material_name": "velvet",
	        "color": "Silver",
	        "country_of_origin": "ITA",
	        "num_units_available": 2
	      }
	    },
	    {
	      "id": "34",
	      "features": {
	      	"product_look_id": 1,
	        "sale_id": 1089811882,
	        "sale_type_key": "multi-brand",
	        "brand_id": "b88",
	        "shoe_type": "bootie",
	        "return_policy_id": "r-1",
	        "base_price": 129.4,
	        "sale_price": 159,
	        "msrp_price": 100,
	        "initial_sale_price": 159,
	        "last_price": 59,
	        "material_name": "velvet",
	        "color": "Silver",
	        "country_of_origin": "ITA",
	        "num_units_available": 2
	      }
	    }
	  ]
	}
	</pre>
	Output: Score for each row
	<pre>
	{
	    "rows": [
	        {
	            "features": {
	                "num_units_sold": 0
	            },
	            "id": "4"
	        },
	        {
	            "features": {
	                "num_units_sold": 1
	            },
	            "id": "14"
	        },
	        {
	            "features": {
	                "errorMessage": "Unable to score this record, invalid data"
	            },
	            "id": "34"
	        }
	    ]
	}
	</pre>


# Developer Knowledge exchange

# Building and Running

# With docker
1. Navigate to *scoring-ms* and run `mvn clean install docker:build`

2. Create a docker container based on the created *rnafde/scoring:1.0* docker image
`docker run -it -p 8080:8080 rnafde/scoring:1.0`

# Standalone JAR
1. Navigate to *scoring-ms* and run `mvn clean install`

2. Navigate to *scoring-ms/target* folder and run
`java -jar ./scoring-rest/target/scoring-rest-1.0.0-SNAPSHOT.jar server "/Users/rahulnafde/Documents/HBC/Solution/scoring/scoring-rest/src/main/resources/scoring.yml"`

## Other Configurations
Currently, the microservice uses *h2* database for maintaining the modelInfo. The configuration for the database is specified in `scoring.yml`. This configuration could also be modified to use *postgresSQL* database.

# References

The microserice uses a list of open source software(s) listed below:

1. JPMML Evaluator (https://github.com/jpmml/jpmml-evaluator) - For model evaluation 
	
1. PMML Model (https://github.com/jpmml/jpmml-model) - To understand and parse a PMML model
